public class WildCat {

    String name;
    double weight; // In kg
    double length; // In cm
    public static final int CM_TO_M = 100;

    public void setName(String name) { 
        this.name = name; 
    }

    public String getName() { 
        return this.name; 
    }

    public void setWeight(double weight) { 
        this.weight = weight; 
    }

    public double getWeight() { 
        return this.weight; 
    }

    public void setLength(double length) { 
        this.length = length; 
    }

    public double getLength() { 
        return this.length; 
    }

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // Method to compute BMI which formula is (weight (kg) / length (cm) squared)
    public double computeMassIndex() {
        double massIndex = this.weight / Math.pow(this.length / CM_TO_M, 2);
        return massIndex;
    }
}