public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kg
    WildCat cat;
    TrainCar next;

    public void setCat(WildCat cat) { 
        this.cat = cat; 
    }

    public WildCat getCat() { 
        return this.cat; 
    }

    public void setNext(TrainCar next) { 
        this.next = next; 
    }

    public TrainCar getNext() { 
        return this.next; 
    } 

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // Method to compute the total weight of linked train cars, recursively
    public double computeTotalWeight() {
        double totalWeight = 0;
        if (this.next == null) {
            totalWeight += TrainCar.EMPTY_WEIGHT + this.cat.weight;
        } else {
            totalWeight += TrainCar.EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
        }
        return totalWeight;
    }

    // Method to compute the total BMI of each linked train car's cat, recursively
    public double computeTotalMassIndex() {
        double totalMassIndex = 0;
        if (this.next == null) {
            totalMassIndex += this.cat.computeMassIndex();
        } else {
            totalMassIndex += this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
        return totalMassIndex;
    }

    // Method to print out the string representation of the train cars, recursively
    public void printCar() {
        if (this.next == null) {
            System.out.print("(" + this.cat.name + ")");
        } else { 
            System.out.print("(" + this.cat.name + ")" + "--");
            this.next.printCar();
        }
    }
}