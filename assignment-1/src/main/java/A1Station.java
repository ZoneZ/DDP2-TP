import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class A1Station {
    // ArrayList that stores TrainCar objects
    static ArrayList<TrainCar> trainList = new ArrayList<>();
    // Array that stores every WildCat objects
    static WildCat[] catList;
    // A counter that tells how many train car is on the departing queue
    static int trainQueue = 0;
    static DecimalFormat decimalFormat = new DecimalFormat("###.##");

    static final double THRESHOLD = 250; // in kilograms

    /* Method that check car's status,
     * if a specific condition is met, 
     * call the depart method 
    */
    public static void queue() {
        // If there's only one cat (one car need)
        if (catList.length == 1) {
            depart(trainList.get(0));
        } else {
            /* If the car contains the last cat,
             * the car becomes the "last car"
             */
            if (trainList.get(trainQueue).cat == catList[catList.length - 1]) {
                depart(trainList.get(trainQueue));
            } else {
                if (trainList.get(trainQueue).computeTotalWeight() > 250) {
                    depart(trainList.get(trainQueue));
                    trainList.clear();
                    trainQueue = 0;
                } else {
                    trainQueue += 1;
                }
            }
        } 
    }

    /* Method to depart the train car with all it's links, if any exists,
     * then print the required informations
     */
    public static void depart(TrainCar car) {
        trainQueue += 1;
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        car.printCar();
        System.out.println();
        System.out.print("Average mass index of all cats: ");
        float averageMassIndex = (float) (car.computeTotalMassIndex() / (trainQueue));
        System.out.println(decimalFormat.format(averageMassIndex));

        String massIndexCategory = "";
        if (averageMassIndex < 18.5) {
            massIndexCategory = "underweight";
        } else {
            if (averageMassIndex < 25) {
                massIndexCategory = "normal";
            } else {
                if (averageMassIndex < 30) {
                    massIndexCategory = "overweight";
                } else {
                    massIndexCategory = "obese"; 
                }
            }
        }
        System.out.format("In average, the cats in the train are *%s* \n", massIndexCategory);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int catNum;

        while (true) {
            System.out.println("Type the number of the cat: ");
            try {
                catNum = input.nextInt();
                if (catNum > 0) {
                    input.nextLine();
                    break;
                } else {
                    System.out.println("The number of the cat cannot be zero or negative!");
                    continue;
                }  
            } catch (Exception exception) {
                input.next();
                System.out.println("Invalid input!");
                continue;
            }
        }
        
        catList = new WildCat[catNum];
        int catCounter = 0;
        while (catCounter < catNum) {
            System.out.format("Type cat #%d specification: ", catCounter + 1);
            try {
                String catSpecs = input.nextLine();
                String[] catSpecsArr = catSpecs.split(",");
                String catName = catSpecsArr[0];
                double catWeight = Double.parseDouble(catSpecsArr[1]);
                double catHeight = Double.parseDouble(catSpecsArr[2]);

                if (catWeight > 0 && catHeight > 0) {
                    WildCat newCat = new WildCat(catName, catWeight, catHeight);
                    catList[catCounter] = newCat;
                    if (trainList.size() == 0) {
                        TrainCar newCar = new TrainCar(catList[catCounter]);
                        trainList.add(newCar);
                    } else {
                        TrainCar newCar = new TrainCar(newCat, trainList.get(trainQueue - 1));
                        trainList.add(newCar);
                    }
                    queue();
                    ++catCounter;
                } else {
                    System.out.println("Cat weight and height can't be zero or negative!");
                    continue;
                }
            } catch (Exception exception) {
                System.out.println("Invalid input!");
                continue;
            } 
        }
    }
}