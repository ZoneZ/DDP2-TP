package utilities;

import animals.Animal;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cage.Cage;
import java.util.ArrayList;

public class AnimalFinder {

    /* Find an animal from the Controller static ArrayList
     * based on their name. If any found, these methods return the "location" 
     * of the said animal from its corresponding ArrayList */

    public static int[] findCat(String name) {
        int[] location = null;
        ArrayList<Cage[]> searchedCages = Controller.getCatList();
        for (int cageLevel = 0; cageLevel < searchedCages.size(); cageLevel++) {
            if (searchedCages.get(cageLevel) == null) {
                continue;
            }
            for (int index = 0; index < searchedCages.get(cageLevel).length; index++) {
                if (searchedCages.get(cageLevel)[index] == null) {
                    continue;
                }
                if (searchedCages.get(cageLevel)[index].getCagedCat()
                                .getName().equalsIgnoreCase(name)) {
                    location = new int[2];
                    location[0] = cageLevel;
                    location[1] = index;
                }
            }
        }
        return location;
    }

    public static int[] findEagle(String name) {
        int[] location = null;
        ArrayList<Cage[]> searchedCages = Controller.getEagleList();
        for (int cageLevel = 0; cageLevel < searchedCages.size(); cageLevel++) {
            if (searchedCages.get(cageLevel) == null) {
                continue;
            }
            for (int index = 0; index < searchedCages.get(cageLevel).length; index++) {
                if (searchedCages.get(cageLevel)[index] == null) {
                    continue;
                }
                if (searchedCages.get(cageLevel)[index].getCagedEagle()
                                .getName().equalsIgnoreCase(name)) {
                    location = new int[2];
                    location[0] = cageLevel;
                    location[1] = index;
                }
            }
        }
        return location;
    }

    public static int[] findHamster(String name) {
        int[] location = null;
        ArrayList<Cage[]> searchedCages = Controller.getHamsterList();
        for (int cageLevel = 0; cageLevel < searchedCages.size(); cageLevel++) {
            if (searchedCages.get(cageLevel) == null) {
                continue;
            }
            for (int index = 0; index < searchedCages.get(cageLevel).length; index++) {
                if (searchedCages.get(cageLevel)[index] == null) {
                    continue;
                }
                if (searchedCages.get(cageLevel)[index].getCagedHamster()
                                .getName().equalsIgnoreCase(name)) {
                    location = new int[2];
                    location[0] = cageLevel;
                    location[1] = index;
                }
            }
        }
        return location;
    }

    public static int[] findParrot(String name) {
        int[] location = null;
        ArrayList<Cage[]> searchedCages = Controller.getParrotList();
        for (int cageLevel = 0; cageLevel < searchedCages.size(); cageLevel++) {
            if (searchedCages.get(cageLevel) == null) {
                continue;
            }
            for (int index = 0; index < searchedCages.get(cageLevel).length; index++) {
                if (searchedCages.get(cageLevel)[index] == null) {
                    continue;
                }
                if (searchedCages.get(cageLevel)[index].getCagedParrot()
                                .getName().equalsIgnoreCase(name)) {
                    location = new int[2];
                    location[0] = cageLevel;
                    location[1] = index;
                }
            }
        }
        return location;
    }

    public static int[] findLion(String name) {
        int[] location = null;
        ArrayList<Cage[]> searchedCages = Controller.getLionList();
        for (int cageLevel = 0; cageLevel < searchedCages.size(); cageLevel++) {
            if (searchedCages.get(cageLevel) == null) {
                continue;
            }
            for (int index = 0; index < searchedCages.get(cageLevel).length; index++) {
                if (searchedCages.get(cageLevel)[index] == null) {
                    continue;
                }
                if (searchedCages.get(cageLevel)[index].getCagedLion()
                                .getName().equalsIgnoreCase(name)) {
                    location = new int[2];
                    location[0] = cageLevel;
                    location[1] = index;
                }
            }
        }
        return location;
    }   
}