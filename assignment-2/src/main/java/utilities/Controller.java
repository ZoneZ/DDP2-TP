package utilities;

import animals.Animal;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cage.Cage;
import java.util.ArrayList;
import java.util.Collections;

public class Controller {

    protected static ArrayList<Cage[]> catList = new ArrayList<>();
    protected static ArrayList<Cage[]> eagleList = new ArrayList<>();
    protected static ArrayList<Cage[]> hamsterList = new ArrayList<>();
    protected static ArrayList<Cage[]> parrotList = new ArrayList<>();
    protected static ArrayList<Cage[]> lionList = new ArrayList<>();
    protected static final int CAGE_LEVELS = 3;

    public static void addCatList(Cage[] cages) {
        Controller.catList.add(cages);
    }
    
    public static void addEagleList(Cage[] cages) {
        Controller.eagleList.add(cages);
    }

    public static void addHamsterList(Cage[] cages) {
        Controller.hamsterList.add(cages);
    }

    public static void addParrotList(Cage[] cages) {
        Controller.parrotList.add(cages);
    }

    public static void addLionList(Cage[] cages) {
        Controller.lionList.add(cages);
    }

    public static ArrayList<Cage[]> getCatList() {
        return Controller.catList;
    }

    public static ArrayList<Cage[]> getEagleList() {
        return Controller.eagleList;
    }

    public static ArrayList<Cage[]> getHamsterList() {
        return Controller.hamsterList;
    }

    public static ArrayList<Cage[]> getParrotList() {
        return Controller.parrotList;
    }

    public static ArrayList<Cage[]> getLionList() {
        return Controller.lionList;
    }

    public static int getNumberofLevel() {
        return Controller.CAGE_LEVELS;
    }

    /* Make single cages by inserting each corresponding animal to the
     * instantiated cage object. The processed cages will be then divided
     * by calling the divideCages() method */
    public static void makeCages(String[] processedCage, String animalType) {
        ArrayList<Cage> tempCages = new ArrayList<>();
        for (int index = 0; index < processedCage.length; index++) {
            String[] tempAnimal = processedCage[index].split("\\|");
            String animalName = tempAnimal[0];
            double animalLength = Double.parseDouble(tempAnimal[1]);

            Cage singleCage;
            if (animalType.equals("Cat")) {
                Cat tempCat = new Cat(animalName, animalLength);
                singleCage = new Cage(tempCat);
            } else if (animalType.equals("Eagle")) {
                Eagle tempEagle = new Eagle(animalName, animalLength);
                singleCage = new Cage(tempEagle);
            } else if (animalType.equals("Hamster")) {
                Hamster tempHamster = new Hamster(animalName, animalLength);
                singleCage = new Cage(tempHamster);
            } else if (animalType.equals("Parrot")) {
                Parrot tempParrot = new Parrot(animalName, animalLength);
                singleCage = new Cage(tempParrot);
            } else {
                Lion tempLion = new Lion(animalName, animalLength);
                singleCage = new Cage(tempLion);
            }
            tempCages.add(singleCage);
        }
        divideCages(tempCages, animalType);
    }

    /* Divide all processed cages with the following rules:
     * If the total number of cages (n) >= 3 :
     * - Level 1 and Level 2 will be filled with floor(n, 3)
     * - Level 3 wil be filled with the remainding cages
     * Otherwise, n < 3:
     * - Fill each Level with one cage each 
     * After dividing the processed cages, store each cage level
     * to the each corresponding static ArrayList by calling the
     * insertCage() method */
    public static void divideCages(ArrayList<Cage> cages, String animalType) {
        int cageSize = cages.size();
        Cage[] tempCageLevel;
        if (cageSize >= Controller.CAGE_LEVELS) {
            int cagePerLevel = cageSize / Controller.CAGE_LEVELS;
            int cageRemainder = cageSize % Controller.CAGE_LEVELS;
            for (int i = 0; i < Controller.CAGE_LEVELS; i++) {
                if (i != Controller.CAGE_LEVELS - 1) {
                    tempCageLevel = new Cage[cagePerLevel];
                } else {
                    tempCageLevel = new Cage[cagePerLevel + cageRemainder];
                }
    
                for (int j = 0; j < tempCageLevel.length; j++) {
                    tempCageLevel[j] = cages.get(0);
                    cages.remove(0);
                }
                insertCage(tempCageLevel, animalType);
            }
        } else {
            for (int num = 0; num < Controller.CAGE_LEVELS - 1; num++) {
                cages.add(null);
            }

            for (int level = 0; level < Controller.CAGE_LEVELS; level++) {
                tempCageLevel = new Cage[Controller.CAGE_LEVELS - 2];
                if (level == 0) {
                    tempCageLevel[0] = cages.get(level);
                } else {
                    if (cages.get(level) != null) {
                        tempCageLevel[0] = cages.get(level);
                    } else {
                        tempCageLevel[0] = null;
                    }
                }
                insertCage(tempCageLevel, animalType);
            }   
        }
    }

    /* Continuation of divideCages() method, this stores a level of cages
     * to the suitable static ArrayList */
    public static void insertCage(Cage[] tempCageLevel, String animalType) {
        if (animalType.equals("Cat")) {
            Controller.catList.add(tempCageLevel);
        } else if (animalType.equals("Eagle")) {
            Controller.eagleList.add(tempCageLevel);
        } else if (animalType.equals("Hamster")) {
            Controller.hamsterList.add(tempCageLevel);
        } else if (animalType.equals("Parrot")) {
            Controller.parrotList.add(tempCageLevel);
        } else {
            Controller.lionList.add(tempCageLevel);
        }
    }

    /* Shifts the cage levels up one level
     * Since no additional levels can be made, 
     * level 3 gets placed at level 1 position */
    public static void swapCages(ArrayList<Cage[]> cages) {
        Collections.swap(cages, 0, 2);
        Collections.swap(cages, 1, 2); 
    }

    // Similiar with palindrome checker algorithm, except this one swaps the two elements
    public static void reverseCages(ArrayList<Cage[]> cages) {
        for (int i = 0; i < cages.size(); i++) {
            for (int j = 0; j < cages.get(i).length / 2; j++) {
                Cage temp = cages.get(i)[j];
                cages.get(i)[j] = cages.get(i)[cages.get(i).length - j - 1];
                cages.get(i)[cages.get(i).length - j - 1] = temp;
            }
        }
    }

    // Prints the contents of one cage level
    public static void printLevels(ArrayList<Cage[]> cages, String animalType) {
        if (cages.size() > 0) {
            for (int level = 0; level < Controller.CAGE_LEVELS; level++) {
                System.out.print("level " + (Controller.CAGE_LEVELS - level) + ": ");
                Cage[] printedLevel = cages.get(2 - level);
                for (int num = 0; num < printedLevel.length; num++) {
                    if (printedLevel[num] != null) {
                        System.out.print(printedLevel[num].toString(animalType) + ", ");
                    }
                }
                System.out.println();
            }
        }
    }

    // "Packs" the essential methods to one handy method 
    public static void manageCagesArrangement(ArrayList<Cage[]> cages, String animalType) {
        printLevels(cages, animalType);
        System.out.println("\nAfter rearrangement...");
        swapCages(cages);
        reverseCages(cages);
        printLevels(cages, animalType);
        System.out.println();
    }
}