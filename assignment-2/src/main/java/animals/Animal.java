package animals;

public class Animal {

    protected String name;
    protected double length;
    protected String cageType;

    public void setName(String name) {
        this.name = name;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setCageType(String type) {
        this.cageType = type;
    }

    public String getName() {
        return this.name;
    }

    public double getLength() {
        return this.length;
    }
    
    public String getCageType() {
        return this.cageType;
    }

    public Animal(String name, double length) {
        this.name = name;
        this.length = length;
    }
}