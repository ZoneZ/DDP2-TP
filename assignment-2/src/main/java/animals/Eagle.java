package animals;

public class Eagle extends Animal {

    private static int numberOfEagles = 0;

    public static int getNumberOfEagles() {
        return Eagle.numberOfEagles;
    }

    public Eagle(String name, double length) {
        super(name, length);
        this.cageType = "Outdoor";
        Eagle.numberOfEagles++;
    }

    public String attack() {
        return "Kwaakk...";
    }
}