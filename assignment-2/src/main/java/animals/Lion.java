package animals;

public class Lion extends Animal {

    private static int numberOfLions = 0;

    public static int getNumberOfLions() {
        return Lion.numberOfLions;
    }

    public Lion(String name, double length) {
        super(name, length);
        this.cageType = "Outdoor";
        Lion.numberOfLions++;
    }

    public String hunt() {
        return "Err....";
    }

    public String goodMood() {
        return "Hauhhmm!";
    }

    public String badMood() {
        return this.goodMood().toUpperCase();
    }
}