package animals;

import java.util.Random;

public class Cat extends Animal {
    private static int numberOfCats = 0;
    private static String[] sounds = {"Miaaaw..",
                                      "Purrr..",
                                      "Mwaw!",
                                      "Mraaawr!"};

    public static int getNumberOfCats() {
        return Cat.numberOfCats;
    }
    
    public static String[] getSounds() {
        return Cat.sounds;
    }
                                      
    public Cat(String name, double length) {
        super(name, length);
        this.cageType = "Indoor";
        Cat.numberOfCats++;
    }

    public String gettingBrushed() {
        return "Nyaaan...";
    }

    public String getCuddled() {
        Random random = new Random();
        int randomSound = random.nextInt(4);
        return Cat.sounds[randomSound];
    }
}