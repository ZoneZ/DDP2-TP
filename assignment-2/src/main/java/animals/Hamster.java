package animals;

public class Hamster extends Animal {

    private static int numberOfHamsters = 0;

    public static int getNumberOfHamsters() {
        return Hamster.numberOfHamsters;
    }

    public Hamster(String name, double length) {
        super(name, length);
        this.cageType = "Indoor";
        Hamster.numberOfHamsters++;
    }

    public String gnaw() {
        return "Ngkkrit.. Ngkkrrriiit";
    }

    public String run() {
        return "Trr... Trr...";
    }
}