package animals;

public class Parrot extends Animal {

    private static int numberOfParrots = 0;

    public static int getNumberOfParrots() {
        return Parrot.numberOfParrots;
    }

    public Parrot(String name, double length) {
        super(name, length);
        this.cageType = "Indoor";
        Parrot.numberOfParrots++;
    }

    public String fly() {
        return "FLYYYY...";
    }

    public String mimicSpecch(String speech) {
        return speech.toUpperCase();
    }

    public String mumble() {
        return "HM?";
    }
}