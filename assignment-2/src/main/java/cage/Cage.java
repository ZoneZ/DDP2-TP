package cage;

import animals.Animal;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;

public class Cage {

    private String size;
    private Cat cagedCat;
    private Lion cagedLion;
    private Eagle cagedEagle;
    private Parrot cagedParrot;
    private Hamster cagedHamster;

    public void setSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return this.size;
    }

    public Cat getCagedCat() {
        return this.cagedCat;
    }

    public Lion getCagedLion() {
        return this.cagedLion;
    }

    public Eagle getCagedEagle() {
        return this.cagedEagle;
    }

    public Parrot getCagedParrot() {
        return this.cagedParrot;
    }

    public Hamster getCagedHamster() {
        return this.cagedHamster;
    }

    public Cage(Cat cat) {
        this.cagedCat = cat;
        this.size = (cagedCat.getLength() < 45) ? "A" :
                        (cagedCat.getLength() < 60) ? "B" : "C";
    }

    public Cage(Lion lion) {
        this.cagedLion = lion;
        this.size = (cagedLion.getLength() < 75) ? "A" :
                        (cagedLion.getLength() < 90) ? "B" : "C";
    }

    public Cage(Eagle eagle) {
        this.cagedEagle = eagle;
        this.size = (cagedEagle.getLength() < 75) ? "A" :
                        (cagedEagle.getLength() < 90) ? "B" : "C";
    }

    public Cage(Parrot parrot) {
        this.cagedParrot = parrot;
        this.size = (cagedParrot.getLength() < 45) ? "A" :
                        (cagedParrot.getLength() < 60) ? "B" : "C";
    }

    public Cage(Hamster hamster) {
        this.cagedHamster = hamster;
        this.size = (cagedHamster.getLength() < 45) ? "A" :
                        (cagedHamster.getLength() < 60) ? "B" : "C";
    }


    public String toString(String animalType) {
        if (animalType.equals("Cat")) {
            return this.cagedCat.getName() + " ( " + this.cagedCat.getLength() 
                + " - " + this.getSize() + " )"; 
        } else if (animalType.equals("Lion")) {
            return this.cagedLion.getName() + " ( " + this.cagedLion.getLength() 
                + " - " + this.getSize() + " )"; 
        } else if (animalType.equals("Eagle")) {
            return this.cagedEagle.getName() + " ( " + this.cagedEagle.getLength() 
                + " - " + this.getSize() + " )"; 
        } else if (animalType.equals("Parrot")) {
            return this.cagedParrot.getName() + " ( " + this.cagedParrot.getLength() 
                + " - " + this.getSize() + " )"; 
        } else {
            return this.cagedHamster.getName() + " ( " + this.cagedHamster.getLength() 
                + " - " + this.getSize() + " )"; 
        }
    }
}