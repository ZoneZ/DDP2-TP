import animals.Animal;
import animals.Cat;
import animals.Eagle;
import animals.Hamster;
import animals.Lion;
import animals.Parrot;
import cage.Cage;
import java.util.ArrayList;
import java.util.Scanner;
import utilities.AnimalFinder;
import utilities.Controller;


public class A2Caretaker {
    public static void main(String[] args) {
        String[] animals = {"Cat", "Lion", "Eagle", "Parrot", "Hamster"};
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari park!");
        System.out.println("Input the number of animals");

        int loops = 0;
        while (loops < animals.length) {
            try {
                System.out.print(animals[loops].toLowerCase() + ": ");
                int numOfAnimal = Integer.parseInt(input.nextLine());
                if (numOfAnimal > 0) {
                    System.out.println("Provide the information of "  
                                        + animals[loops].toLowerCase() + "(s):");
                    String animalSpecs = input.nextLine();
                    String[] animalSpecsArr = animalSpecs.split(",");
                    if (animalSpecsArr.length == numOfAnimal) {
                        Controller.makeCages(animalSpecsArr, animals[loops]);
                        loops++;
                    } else {
                        System.out.println("The number of animals you entered does not match!");
                    }
                } else if (numOfAnimal < 0) {
                    System.out.println("Number of animals cannot be negative!");
                } else {
                    loops++;
                } 
            } catch (Exception e) {
                System.out.println("Invalid input! Try again");
            }
        }

        if (Cat.getNumberOfCats() + Eagle.getNumberOfEagles() + Hamster.getNumberOfHamsters() 
            + Lion.getNumberOfLions() + Parrot.getNumberOfParrots() == 0) {
            System.out.println("There are no animals at all! Congratulations!");
            System.exit(0);
        }

        System.out.println("Animals have been successfully recorded!\n\n");
        System.out.println("=============================================");
        System.out.println("Cage arrangement:");

        if (Controller.getCatList().size() > 0) {
            System.out.println("location: " + Controller
                                .getCatList().get(0)[0].getCagedCat().getCageType());
            Controller.manageCagesArrangement(Controller.getCatList(), "Cat");
        }

        if (Controller.getLionList().size() > 0) {
            System.out.println("location: " + Controller
                                .getLionList().get(0)[0].getCagedLion().getCageType());
            Controller.manageCagesArrangement(Controller.getLionList(), "Lion");
        }
        
        if (Controller.getEagleList().size() > 0) {
            System.out.println("location: " + Controller
                                .getEagleList().get(0)[0].getCagedEagle().getCageType());
            Controller.manageCagesArrangement(Controller.getEagleList(), "Eagle");
        }

        if (Controller.getParrotList().size() > 0) {
            System.out.println("location: " + Controller
                                .getParrotList().get(0)[0].getCagedParrot().getCageType());
            Controller.manageCagesArrangement(Controller.getParrotList(), "Parrot");
        }

        if (Controller.getHamsterList().size() > 0) {
            System.out.println("location: " + Controller
                                .getHamsterList().get(0)[0].getCagedHamster().getCageType());
            Controller.manageCagesArrangement(Controller.getHamsterList(), "Hamster");
        }

        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat: " + Cat.getNumberOfCats());
        System.out.println("lion: " + Lion.getNumberOfLions());
        System.out.println("parrot: " + Parrot.getNumberOfParrots());
        System.out.println("eagle: " + Eagle.getNumberOfEagles());
        System.out.println("hamster: " + Hamster.getNumberOfHamsters() + "\n");

        System.out.println("=============================================");
        while (true) {
            try {
                System.out.println("Which animal do you want to visit?");
                System.out.println("1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit");
                int choice = Integer.parseInt(input.nextLine());
                if (choice == 1) {
                    if (Controller.getCatList().size() == 0) {
                        System.out.println("There are no cats! Back to the office!\n");
                    } else {
                        System.out.print("Mention the name of cat you want to visit: ");
                        String catName = input.nextLine();
                        if (AnimalFinder.findCat(catName) != null) {
                            int[] catLocation = AnimalFinder.findCat(catName);
                            Cat catObject = Controller.getCatList()
                                            .get(catLocation[0])[catLocation[1]].getCagedCat();
                            System.out.format("You are visiting %s (cat) now" 
                                              + " what would you like to do?\n", catName);
                            System.out.println("1: Brush the fur, 2: Cuddle");
                            int action = Integer.parseInt(input.nextLine());
                            if (action == 1) {
                                System.out.format("Time to clean %s's fur\n", catName);
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                catName, catObject.gettingBrushed());
                            } else if (action == 2) {
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                catName, catObject.getCuddled());
                            } else {
                                System.out.println("You do nothing... \nBack to the office!\n");
                            }
                        } else {
                            System.out.println("There is no cat with that name!"
                                               + " Back to the office!\n");
                        }
                    }

                } else if (choice == 2) {
                    if (Controller.getEagleList().size() == 0) {
                        System.out.println("There are no eagles! Back to the office!\n");
                    } else {
                        System.out.print("Mention the name of eagle you want to visit: ");
                        String eagleName = input.nextLine();
                        if (AnimalFinder.findEagle(eagleName) != null) {
                            int[] eagleLocation = AnimalFinder.findEagle(eagleName);
                            Eagle eagleObject = Controller.getEagleList()
                                                .get(eagleLocation[0])[eagleLocation[1]]
                                                .getCagedEagle();
                            System.out.format("You are visiting %s (eagle) now," 
                                              + " what would you like to do?\n", eagleName);
                            System.out.println("1: Order to fly");
                            int action = Integer.parseInt(input.nextLine());
                            if (action == 1) {
                                System.out.format("%s makes a voice: %s\n",
                                                eagleName, eagleObject.attack());
                                System.out.println("You are hurt! \nBack to the office!\n");
                            } else {
                                System.out.println("You do nothing... \nBack to the office!\n");
                            }
                        } else {
                            System.out.println("There is no eagle with that name!" 
                                               + "Back to the office!\n");
                        }
                    }


                } else if (choice == 3) {
                    if (Controller.getHamsterList().size() == 0) {
                        System.out.println("There are no hamsters! Back to the office!\n");
                    } else {
                        System.out.print("Mention the name of hamster you want to visit: ");
                        String hamsterName = input.nextLine();
                        if (AnimalFinder.findHamster(hamsterName) != null) {
                            int[] hamsterLocation = AnimalFinder.findHamster(hamsterName);
                            Hamster hamsterObject = Controller.getHamsterList()
                                                    .get(hamsterLocation[0])[hamsterLocation[1]]
                                                    .getCagedHamster();
                            System.out.format("You are visiting %s (hamster) now," 
                                              + " what would you like to do?\n", hamsterName);
                            System.out.println("1: See it gnawing" 
                                               + " 2: Order to run in the hamster wheel");
                            int action = Integer.parseInt(input.nextLine());
                            if (action == 1) {
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                hamsterName, hamsterObject.gnaw());
                            } else if (action == 2) {
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                hamsterName, hamsterObject.run());
                            } else {
                                System.out.println("You do nothing... \nBack to the office!\n");
                            }
                        } else {
                            System.out.println("There is no hamster with that name!"
                                               + " Back to the office!\n");
                        }
                    }

                } else if (choice == 4) {
                    if (Controller.getParrotList().size() == 0) {
                        System.out.println("There are no parrots! Back to the office!\n");
                    } else {
                        System.out.print("Mention the name of parrot you want to visit: ");
                        String parrotName = input.nextLine();
                        if (AnimalFinder.findParrot(parrotName) != null) {
                            int[] parrotLocation = AnimalFinder.findParrot(parrotName);
                            Parrot parrotObject = Controller.getParrotList()
                                                        .get(parrotLocation[0])[parrotLocation[1]]
                                                        .getCagedParrot();
                            System.out.format("You are visiting %s (parrot) now,"
                                              + " what would you like to do?\n", parrotName);
                            System.out.println("1: Order to fly 2: Do conversation");
                            int action = Integer.parseInt(input.nextLine());
                            if (action == 1) {
                                System.out.format("Parrot %s flies!\n", parrotName);
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                parrotName, parrotObject.fly());
                            } else if (action == 2) {
                                System.out.print("You say: ");
                                String speech = input.nextLine();
                                System.out.format("%s says: %s\nBack to the office!\n\n",
                                                parrotName, parrotObject.mimicSpecch(speech));
                            } else {
                                System.out.format("%s says: %s\nBack to the office!\n\n",
                                                parrotName, parrotObject.mumble());
                            }
                        } else {
                            System.out.println("There is no parrot with that name!"
                                               + " Back to the office!\n");
                        }
                    }
                    
                } else if (choice == 5) {
                    if (Controller.getLionList().size() == 0) {
                        System.out.println("There are no lions! Back to the office!\n");
                    } else {
                        System.out.print("Mention the name of lion you want to visit: ");
                        String lionName = input.nextLine();
                        if (AnimalFinder.findLion(lionName) != null) {
                            int[] lionLocation = AnimalFinder.findLion(lionName);
                            Lion lionObject = Controller.getLionList()
                                              .get(lionLocation[0])[lionLocation[1]]
                                              .getCagedLion();
                            System.out.format("You are visiting %s (lion) now,"
                                              + " what would you like to do?\n", lionName);
                            System.out.println("1: See it hunting 2: Brush the mane"
                                               + " 3: Disturb it");
                            int action = Integer.parseInt(input.nextLine());
                            if (action == 1) {
                                System.out.println("Lion is hunting..");
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                lionName, lionObject.hunt());
                            } else if (action == 2) {
                                System.out.println("Clean the lion's mane..");
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                lionName, lionObject.goodMood());
                            } else if (action == 3) {
                                System.out.format("%s makes a voice: %s\nBack to the office!\n\n",
                                                lionName, lionObject.badMood());
                            } else {
                                System.out.println("You do nothing... \nBack to the office!\n");
                            }
                        } else {
                            System.out.println("There is no lion with that name!"
                                               + " Back to the office!\n");
                        }
                    }

                } else if (choice == 99) {
                    System.out.println("Thank you for visiting Javari Park!");
                    System.exit(0);
                } else {
                    System.out.println("Command not recognized\n");
                }
            } catch (Exception e) {
                System.out.println("Invalid input! Try again\n");
            }
        }

    }
}