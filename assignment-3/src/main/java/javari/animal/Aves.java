package javari.animal;

/**
 * This class represents common attributes and behaviours found in all Aves in Javari Park.
 */

public class Aves extends Animal {

    private String specialCondition;

    public void setSpecialCondition(String specialCondition) {
        this.specialCondition = specialCondition;
    }

    public String getSpecialCondition() {
        return this.specialCondition;
    }

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String specialCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = "not laying eggs";
    }

    @Override
    protected boolean specificCondition() {
        boolean out;
        if (this.specialCondition.equalsIgnoreCase("laying eggs")) {
            out = false;
        } else {
            out = true;
        }
        return out;
    }
}