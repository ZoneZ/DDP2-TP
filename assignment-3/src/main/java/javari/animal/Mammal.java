package javari.animal;

/**
 * This class represents common attributes and behaviours found in all Mammals in Javari Park.
 */

public class Mammal extends Animal {

    private String specialCondition;

    public void setSpecialCondition(String specialCondition) {
        this.specialCondition = specialCondition;
    }

    public String getSpecialCondition() {
        return this.specialCondition;
    }

    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, String specialCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = "not pregnant";
    }

    @Override
    protected boolean specificCondition() {
        boolean out;
        if (this.specialCondition.equalsIgnoreCase("pregnant")) {
            out = false;
        } else {
            if (this.getType().equals("Lion")) {
                if (this.getGender() == Gender.FEMALE) {
                    out = false;
                } else {
                    out = true;
                }
            } else {
                out = true;
            }
        }
        return out;
    }
}