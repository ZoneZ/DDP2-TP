package javari.animal;

/**
 * This class represents common attributes and behaviours found in all Reptiles in Javari Park.
 */

public class Reptile extends Animal {

    private String specialCondition;

    public void setSpecialCondition(String specialCondition) {
        this.specialCondition = specialCondition;
    }

    public String getSpecialCondition() {
        return this.specialCondition;
    }

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, String specialCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = "tame";
    }

    @Override
    protected boolean specificCondition() {
        boolean out;
        if (this.specialCondition.equalsIgnoreCase("wild")) {
            out = false;
        } else {
            out = true;
        }
        return out;
    }
}