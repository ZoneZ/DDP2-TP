package javari.park;

import java.util.ArrayList;
import java.util.List;

import javari.animal.Animal;


/**
 * This class holds all information of sections, attractions, categories,
 * and animals available in Javari Park.
 */

public class Park {

    private static List<String> sections = new ArrayList<>();
    private static List<SelectedAttraction> attractions = new ArrayList<>();
    private static List<String> categories = new ArrayList<>();
    private static List<Animal> animals = new ArrayList<>();

    public static void addSection(String section) {
        sections.add(section);
    }

    public static List<String> getSections() {
        return sections;
    }

    public static void addAttraction(SelectedAttraction attraction) {
        attractions.add(attraction);
    }

    public static List<SelectedAttraction> getAttractions() {
        return attractions;
    }

    public static void addCategory(String category) {
        categories.add(category);
    }

    public static List<String> getCategories() {
        return categories;
    }

    public static void addAnimal(Animal animal) {
        animals.add(animal);
    }

    public static List<Animal> getAnimals() {
        return animals;
    }
}
