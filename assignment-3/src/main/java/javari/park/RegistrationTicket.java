package javari.park;

import java.util.List;

/**
 * Class that represents a registration ticket which holds ID, visitor's name,
 * and the selected attraction(s).
 */

public class RegistrationTicket implements Registration {

    private int registrationId;
    private String visitorName;
    private List<SelectedAttraction> selectedAttractions;

    public RegistrationTicket(int registrationId, String visitorName,
                              List<SelectedAttraction> selectedAttractions) {
        this.registrationId = registrationId;
        this.visitorName = visitorName;
        this.selectedAttractions = selectedAttractions;
    }

    @Override
    public int getRegistrationId() {
        return this.registrationId;
    }

    @Override
    public String getVisitorName() {
        return this.visitorName;
    }

    @Override
    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttractions;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        boolean out = false;
        boolean foundDuplicateAttraction = false;
        for (SelectedAttraction attraction : this.selectedAttractions) {
            if (attraction.getName().equals(selected.getName())) {
                out = false;
                foundDuplicateAttraction = true;
            }
        }
        if (!foundDuplicateAttraction) {
            if (selected.getPerformers().size() != 0) {
                out = true;
                this.selectedAttractions.add(selected);
            }
        }
        return out;
    }
}