package javari.park;

import java.util.ArrayList;
import java.util.List;

import javari.animal.Animal;

/**
 * This class describes the attributes and behaviours of Passionate Coders attraction.
 */

public class PassionateCoders implements SelectedAttraction {

    private static final String name = "Passionate Coders";
    private String type;
    private List<Animal> performers;

    public PassionateCoders(String type) {
        this.type = type;
        this.performers = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public List<Animal> getPerformers() {
        return this.performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            this.performers.add(performer);
            return true;
        } else {
            return false;
        }
    }
}
