package javari;

/**
 * Class that shows all registration menus
 */

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Mammal;
import javari.animal.Reptile;
import javari.park.CircleOfFire;
import javari.park.CountingMasters;
import javari.park.DancingAnimals;
import javari.park.Park;
import javari.park.PassionateCoders;
import javari.park.RegistrationTicket;
import javari.park.SelectedAttraction;
import javari.writer.RegistrationWriter;

public class RegistrationMenus {

    private static int registrationId = 0;

    /**
     * Shows all available section(s) from Javari Park.
     */
    public static void sectionMenu() {
        int num = 0;
        List<String> sections = Park.getSections();

        // If there is no attraction available
        if (Park.getSections().size() == 0) {
            System.out.println("There are no sections at all");
            System.exit(0);
        }

        System.out.format("Javari Park has %d sections:\n", Park.getSections().size());
        for (String section : sections) {
            System.out.format("%d. %s\n", ++num, section);
        }

        System.out.print("Please choose your preferred section (type the number): ");
        Scanner input = new Scanner(System.in);
        String choice = input.nextLine();
        if (choice.equals("#")) {
            System.out.println("Thank you for visiting Javari Park!");
            System.exit(0);
        } else if (Integer.parseInt(choice) <= num) {
            if (sections.get(Integer.parseInt(choice) - 1).equals("Explore the Mammals")) {
                RegistrationMenus.mammalMenu();
            } else if (sections.get(Integer.parseInt(choice) - 1).equals("World of Aves")) {
                RegistrationMenus.avesMenu();
            } else if (sections.get(Integer.parseInt(choice) - 1).equals("Reptillian Kingdom")) {
                RegistrationMenus.reptileMenu();
            }
        } else {
            System.out.println("Invalid command");
            RegistrationMenus.sectionMenu();
        }
    }

    /**
     * Shows all available mammal(s) from Javari Park.
     */
    public static void mammalMenu() {
        String[] animalTypes = {"Hamster", "Lion", "Cat", "Whale"};
        Scanner input = new Scanner(System.in);
        int showableAnimals = 0;
        System.out.println("--Explore the Mammals--\n" + "1. Hamster\n" + "2. Lion\n"
                + "3. Cat\n" + "4. Whale");
        System.out.print("Please choose your preferred animal (type the number): ");
        String choice = input.nextLine();
        System.out.println();

        if (choice.equals("#")) {
            RegistrationMenus.sectionMenu();
        }
        for (Animal animal : Park.getAnimals()) {
            if (Integer.parseInt(choice) == 1) {
                if (animal instanceof Mammal && animal.getType().equals("Hamster")
                        && animal.isShowable()) {
                    showableAnimals++;
                }
            } else if (Integer.parseInt(choice) == 2) {
                if (animal instanceof Mammal && animal.getType().equals("Lion")
                        && animal.isShowable()) {
                    showableAnimals++;
                }
            } else if (Integer.parseInt(choice) == 3) {
                if (animal instanceof Mammal && animal.getType().equals("Cat")
                        && animal.isShowable()) {
                    showableAnimals++;
                }
            } else if (Integer.parseInt(choice) == 4) {
                if (animal instanceof Mammal && animal.getType().equals("Whale")
                        && animal.isShowable()) {
                    showableAnimals++;
                }
            }
        }

        // If there are no eligible/available mammals of the selected mammal type
        if (showableAnimals == 0) {
            System.out.format("Unfortunately, no %s can perform any attraction,"
                    + " please choose other animals\n\n",
                    animalTypes[Integer.parseInt(choice) - 1]);
            RegistrationMenus.sectionMenu();
        } else {
            RegistrationMenus.attractionsMenu(animalTypes[Integer.parseInt(choice) - 1]);
        }
    }

    /**
     * Shows all available avis from Javari Park.
     */
    public static void avesMenu() {
        String[] animalTypes = {"Eagle", "Parrot"};
        int showableAnimals = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("--World of Aves--\n" + "1. Eagle\n" + "2. Parrot");
        System.out.print("Please choose your preferred animal (type the number): ");
        String choice = input.nextLine();
        System.out.println();
        if (choice.equals("#")) {
            RegistrationMenus.sectionMenu();
        }
        for (Animal animal : Park.getAnimals()) {
            if (Integer.parseInt(choice) == 1) {
                if (animal instanceof Aves && animal.getType().equals("Eagle")
                        && animal.isShowable()) {
                    showableAnimals++;
                }
            } else if (Integer.parseInt(choice) == 2) {
                if (animal instanceof Aves && animal.getType().equals("Parrot")
                        && animal.isShowable()) {
                    showableAnimals++;
                }
            }
        }

        // If there are no eligible/available avis of the selected aves type
        if (showableAnimals == 0) {
            System.out.format("Unfortunately, no %s can perform any attraction,"
                    + " please choose other animals\n\n",
                    animalTypes[Integer.parseInt(choice) - 1]);
            RegistrationMenus.sectionMenu();
        } else {
            RegistrationMenus.attractionsMenu(animalTypes[Integer.parseInt(choice) - 1]);
        }
    }

    /**
     * Shows all available reptile(s).
     */
    public static void reptileMenu() {
        int showableAnimals = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("--Reptillian Kingdom--\n" + "1. Snake");
        System.out.print("Please choose your preferred animal (type the number): ");
        String choice = input.nextLine();
        System.out.println();
        if (choice.equals("#")) {
            RegistrationMenus.sectionMenu();
        }
        for (Animal animal : Park.getAnimals()) {
            if (animal instanceof Reptile && animal.isShowable()) {
                showableAnimals++;
            }
        }

        // If there are no eligible/available reptiles of the selected reptile type
        if (showableAnimals == 0) {
            System.out.println("Unfortunately, no snake can perform any attraction\n\n");
            RegistrationMenus.sectionMenu();
        } else {
            RegistrationMenus.attractionsMenu("Snake");
        }
    }

    /**
     * Shows every available attraction(s) than can be done by the selected animal type.
     *
     * @param animalType : animal type whose attraction(s) are to be shown
     */
    public static void attractionsMenu(String animalType) {
        Set<String> doableAttractions = new HashSet<>();
        if (animalType.equals("Cat") || animalType.equals("Snake")) {
            for (SelectedAttraction attraction : Park.getAttractions()) {
                if ((attraction.getName().equals("Dancing Animals")
                        || attraction.getName().equals("Passionate Coders"))
                        && attraction.getType().equals(animalType)) {
                    doableAttractions.add(attraction.getName());
                }
            }

        } else if (animalType.equals("Lion") || animalType.equals("Eagle")) {
            for (SelectedAttraction attraction : Park.getAttractions()) {
                if (attraction.getName().equals("Circle of Fire")
                        && attraction.getType().equals(animalType)) {
                    doableAttractions.add(attraction.getName());
                }
            }

        } else if (animalType.equals("Parrot")) {
            for (SelectedAttraction attraction : Park.getAttractions()) {
                if ((attraction.getName().equals("Dancing Animals"))
                        || attraction.getName().equals("Counting Masters")
                        && attraction.getType().equals(animalType)) {
                    doableAttractions.add(attraction.getName());
                }
            }
        } else if (animalType.equals("Hamster")) {
            for (SelectedAttraction attraction : Park.getAttractions()) {
                if ((attraction.getName().equals("Dancing Animals"))
                        || attraction.getName().equals("Counting Masters")
                        || attraction.getName().equals("Passionate Coders")
                        && attraction.getType().equals(animalType)) {
                    doableAttractions.add(attraction.getName());
                }
            }

        } else if (animalType.equals("Whale")) {
            for (SelectedAttraction attraction : Park.getAttractions()) {
                if ((attraction.getName().equals("Circle of Fire")
                        || attraction.getName().equals("Counting Masters"))
                        && attraction.getType().equals(animalType)) {
                    doableAttractions.add(attraction.getName());
                }
            }
        }

        //If there are attractions available for the selected animal type
        if (doableAttractions.size() == 0) {
            System.out.format("There are no attractions available for this animal\n");
            RegistrationMenus.sectionMenu();
        }

        System.out.format("--%s--\n Attractions by %s\n", animalType, animalType);
        List<String> attractions = new ArrayList<>(doableAttractions);
        for (int i = 0; i < attractions.size(); i++) {
            System.out.format("%d. %s\n", i + 1, attractions.get(i));
        }

        System.out.println("Please choose your preferred attraction (type the number): ");
        Scanner input = new Scanner(System.in);
        String choice = input.nextLine();
        if (choice.equals("#")) {
            RegistrationMenus.sectionMenu();
        } else if (Integer.parseInt(choice) <= doableAttractions.size()) {
            RegistrationMenus.addEligibleAnimals(animalType,
                    attractions.get(Integer.parseInt(choice) - 1));
        } else {
            System.out.println("Invalid command");
            RegistrationMenus.attractionsMenu(animalType);
        }
    }

    /**
     * Checks if any animal from the selected animal type can perform the selected attraction.
     *
     * @param animalType : animal type selected
     * @param attraction : attraction selected
     */
    public static void addEligibleAnimals(String animalType, String attraction) {
        List<Animal> showableAnimals = new ArrayList<>();
        for (Animal animal : Park.getAnimals()) {
            if (animal.getType().equals(animalType)) {
                if (animal.isShowable()) {
                    showableAnimals.add(animal);
                }
            }
        }
        RegistrationMenus.verificationMenu(animalType, attraction, showableAnimals);
    }

    /**
     * Shows verification menu. In the end, creates a ticket object and process it
     * to a JSON file
     *
     * @param animalType : animal type that is to be printed on the ticket
     * @param attraction : attraction that is to be printed on the ticket
     * @param animals    : list of eligible animals to do the selected attraction
     */
    public static void verificationMenu(String animalType, String attraction,
                                        List<Animal> animals) {
        List<SelectedAttraction> attractions = new ArrayList<>();
        SelectedAttraction selected;
        if (attraction.equals("Circle of Fire")) {
            selected = new CircleOfFire(animalType);
        } else if (attraction.equals("Dancing Animals")) {
            selected = new DancingAnimals(animalType);
        } else if (attraction.equals("Counting Masters")) {
            selected = new CountingMasters(animalType);
        } else {
            selected = new PassionateCoders(animalType);
        }
        for (Animal animal : animals) {
            selected.addPerformer(animal);
        }
        Scanner input = new Scanner(System.in);
        System.out.print("Wow, one more step,\nplease let us know your name: ");
        String visitorName = input.nextLine();
        System.out.println("\nYeay, final check!\nHere is your data, and the attraction"
                + " you chose:");
        System.out.format("Name: %s\nAttraction: %s -> %s\n", visitorName, attraction, animalType);
        String animalNames = "";
        for (int i = 0; i < selected.getPerformers().size(); i++) {
            animalNames += selected.getPerformers().get(i).getName();
            if (i != selected.getPerformers().size() - 1) {
                animalNames += ", ";
            }
        }
        System.out.format("With: %s\n", animalNames);
        System.out.print("Is the data correct? (Y/N): ");
        String confirmation = input.nextLine();

        if (confirmation.toUpperCase().equals("Y")) {
            attractions.add(selected);
            RegistrationTicket selectedTicket = null;

            for (RegistrationTicket ticket : A3Festival.getTickets()) {
                if (ticket.getVisitorName().equalsIgnoreCase(visitorName)) {
                    selectedTicket = ticket;
                }
            }

            if (selectedTicket == null) {
                selectedTicket = new RegistrationTicket(++RegistrationMenus.registrationId,
                        visitorName, attractions);
                A3Festival.addTicket(selectedTicket);
            } else {
                selectedTicket.addSelectedAttraction(selected);
            }

            Path directory = Paths.get(System.getProperty("user.dir"));
            try {
                RegistrationWriter.writeJson(selectedTicket, directory);
            } catch (IOException e) {
                System.out.println("There was something wrong");
            }

            System.out.print("Thank you for your interest. "
                    + "Would you like to register to other attractions? (Y/N) ");
            String choice = input.nextLine();

            if (choice.toUpperCase().equals("Y")) {
                RegistrationMenus.sectionMenu();
            } else if (choice.toUpperCase().equals("N")) {
                System.out.println("End of program... Thank you for visiting Javari Park!");
                System.exit(0);
            } else {
                System.out.println("Invalid input");
                RegistrationMenus.sectionMenu();
            }

        } else if (confirmation.toUpperCase().equals("N")) {
            for (Animal animal : selected.getPerformers()) {
                Park.addAnimal(animal);
            }
            System.out.println();
            RegistrationMenus.attractionsMenu(animalType);
        } else {
            System.out.println("Invalid input");
            RegistrationMenus.attractionsMenu(animalType);
        }
    }
}