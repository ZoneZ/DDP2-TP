package javari;

import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javari.park.RegistrationTicket;
import javari.reader.AttractionsReader;
import javari.reader.CategoriesReader;
import javari.reader.RecordReader;


public class A3Festival {

    private static List<RegistrationTicket> tickets = new ArrayList<>();

    public static List<RegistrationTicket> getTickets() {
        return tickets;
    }

    public static void addTicket(RegistrationTicket ticket) {
        tickets.add(ticket);
    }

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        CategoriesReader ctgReader = null;
        AttractionsReader attReader = null;
        RecordReader rcdReader = null;

        /* Asks for a directory to process animals_categories.csv, animals_attractions.csv,
         * and animals_records.csv
         */
        while (true) {
            try {
                Scanner directoryInput = new Scanner(System.in);
                System.out.print("Please provide the source data path: ");
                String directory = directoryInput.nextLine();

                ctgReader = new CategoriesReader(Paths.get(directory + "\\animals_categories.csv"));
                for (String line : ctgReader.getLines()) {
                    ctgReader.lineProcessor(line);
                }

                attReader = new AttractionsReader(
                        Paths.get(directory + "\\animals_attractions.csv"));
                for (String line : attReader.getLines()) {
                    attReader.lineProcessor(line);
                }

                rcdReader = new RecordReader(
                        Paths.get(directory + "\\animals_records.csv"));
                for (String line : rcdReader.getLines()) {
                    rcdReader.lineProcessor(line);
                }


                System.out.println("... Loading... Success... System is populating data...");

                System.out.format("Found %d valid sections and %d invalid sections\n",
                        ctgReader.getValidSectionCounter(), ctgReader.getInvalidSectionCounter());
                System.out.format("Found %d valid attractions and %d invalid attractions\n",
                        attReader.getValidAttractionCounter(),
                        attReader.getInvalidAttractionCounter());
                System.out.format("Found %d valid animal categories and %d "
                                + " invalid animal categories\n",
                        ctgReader.getValidCategoryCounter(),
                        ctgReader.getInvalidCategoryCounter());
                System.out.format("Found %d valid animal records and %d invalid animal records\n",
                        rcdReader.getValidRecordCounter(), rcdReader.getInvalidRecordCounter());
                break;
            } catch (Exception e) {
                System.out.println("File(s) not found or invalid input detected");
            }
        }

        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println("Please answer the questions by typing the number."
                + " Type # if you want to return to the previous menu");

        while (true) {
            try {
                RegistrationMenus.sectionMenu();
            } catch (Exception e) {
                System.out.println("There was something wrong or invalid input detected");
            }
        }
    }
}