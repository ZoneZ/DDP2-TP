package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import javari.animal.Aves;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammal;
import javari.animal.Reptile;
import javari.park.Park;

/**
 * Class that process an animals_records.csv file
 * and creates Animal objects according to every valid data
 */
public class RecordReader extends CsvReader {

    private long validRecordCounter;
    private long invalidRecordCounter;

    public long getValidRecordCounter() {
        return validRecordCounter;
    }

    public long getInvalidRecordCounter() {
        return invalidRecordCounter;
    }

    public RecordReader(Path file) throws IOException {
        super(file);
        this.validRecordCounter = 0;
        this.invalidRecordCounter = 0;
    }

    public void lineProcessor(String line) {
        String[] lineSplit = line.split(",", -1);
        int animalId = Integer.parseInt(lineSplit[0]);
        String animalType = lineSplit[1];
        String animalName = lineSplit[2];
        String animalGender = lineSplit[3];
        double animalLength = Double.parseDouble(lineSplit[4]);
        double animalWeight = Double.parseDouble(lineSplit[5]);
        String animalSpecialStatus = lineSplit[6];
        String animalCondition = lineSplit[7];

        if (!animalType.equals("Cat")
                && !animalType.equals("Lion")
                && !animalType.equals("Hamster")
                && !animalType.equals("Whale")
                && !animalType.equals("Eagle")
                && !animalType.equals("Parrot")
                && !animalType.equals("Snake")) {
            this.invalidRecordCounter++;
        } else {
            if ((!(animalGender.equals("male")) && !(animalGender.equals("female")))
                    || (!(animalCondition.equals("healthy"))
                    && !(animalCondition.equals("not healthy")))
                    || animalLength <= 0 || animalWeight <= 0) {
                this.invalidRecordCounter++;
            } else {
                if (animalType.equals("Cat") || animalType.equals("Hamster")
                        || animalType.equals("Lion") || animalType.equals("Whale")) {
                    if (animalSpecialStatus != null) {
                        Park.addAnimal(new Mammal(animalId, animalType, animalName,
                                Gender.parseGender(animalGender), animalLength, animalWeight,
                                Condition.parseCondition(animalCondition), animalSpecialStatus));
                        validRecordCounter++;
                    } else {
                        Park.addAnimal(new Mammal(animalId, animalType, animalName,
                                Gender.parseGender(animalGender), animalLength, animalWeight,
                                Condition.parseCondition(animalCondition)));
                    }
                } else if (animalType.equals("Eagle") || animalType.equals("Parrot")) {
                    if (animalSpecialStatus != null) {
                        Park.addAnimal(new Aves(animalId, animalType, animalName,
                                Gender.parseGender(animalGender), animalLength, animalWeight,
                                Condition.parseCondition(animalCondition), animalSpecialStatus));
                        validRecordCounter++;
                    } else {
                        Park.addAnimal(new Aves(animalId, animalType, animalName,
                                Gender.parseGender(animalGender), animalLength, animalWeight,
                                Condition.parseCondition(animalCondition)));
                        validRecordCounter++;
                    }
                } else if (animalType.equals("Snake")) {
                    if (animalSpecialStatus != null) {
                        Park.addAnimal(new Reptile(animalId, animalType, animalName,
                                Gender.parseGender(animalGender), animalLength, animalWeight,
                                Condition.parseCondition(animalCondition), animalSpecialStatus));
                        validRecordCounter++;
                    } else {
                        Park.addAnimal(new Reptile(animalId, animalType, animalName,
                                Gender.parseGender(animalGender), animalLength, animalWeight,
                                Condition.parseCondition(animalCondition), animalSpecialStatus));
                        validRecordCounter++;
                    }
                }
            }
        }
    }
}