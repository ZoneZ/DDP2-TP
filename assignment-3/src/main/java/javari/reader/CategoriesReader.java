package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

import javari.park.Park;


/**
 * Class that process an animals_categories.csv file
 */
public class CategoriesReader extends CsvReader {

    private long validSectionCounter;
    private long invalidSectionCounter;
    private long validCategoryCounter;
    private long invalidCategoryCounter;

    public CategoriesReader(Path file) throws IOException {
        super(file);
        this.validSectionCounter = 0;
        this.invalidSectionCounter = 0;
        this.validCategoryCounter = 0;
        this.invalidCategoryCounter = 0;
    }

    public long getValidSectionCounter() {
        return validSectionCounter;
    }

    public long getValidCategoryCounter() {
        return validCategoryCounter;
    }

    public long getInvalidSectionCounter() {
        return invalidSectionCounter;
    }

    public long getInvalidCategoryCounter() {
        return invalidCategoryCounter;
    }

    public void lineProcessor(String line) {
        String[] lineSplit = line.split(",");
        String animalCategory = lineSplit[1];
        String animalSection = lineSplit[2];

        if (animalSection.equalsIgnoreCase("Explore the Mammals")
                && !Park.getSections().contains("Explore the Mammals")) {
            Park.addSection("Explore the Mammals");
            this.validSectionCounter++;
        } else if (animalSection.equalsIgnoreCase("World of Aves")
                && !Park.getSections().contains("World of Aves")) {
            Park.addSection("World of Aves");
            this.validSectionCounter++;
        } else if (animalSection.equalsIgnoreCase("Reptillian Kingdom")
                && !Park.getSections().contains("Reptillian Kingdom")) {
            Park.addSection("Reptillian Kingdom");
            this.validSectionCounter++;
        } else if (!animalSection.equalsIgnoreCase("Explore the Mammals")
                && !animalSection.equalsIgnoreCase("World of Aves")
                && !animalSection.equalsIgnoreCase("Reptillian Kingdom")) {
            this.invalidSectionCounter++;
        }

        if (animalCategory.equalsIgnoreCase("mammals")
                && !Park.getCategories().contains("Mammals")) {
            Park.addCategory("Mammals");
            this.validCategoryCounter++;
        } else if (animalCategory.equalsIgnoreCase("aves")
                && !Park.getCategories().contains("Aves")) {
            Park.addCategory("Aves");
            this.validCategoryCounter++;
        } else if (animalCategory.equalsIgnoreCase("reptiles")
                && !Park.getCategories().contains("Reptiles")) {
            Park.addCategory("Reptiles");
            this.validCategoryCounter++;
        } else if (!(animalCategory.equalsIgnoreCase("mammals"))
                && !(animalCategory.equalsIgnoreCase("aves"))
                && !(animalCategory.equalsIgnoreCase("reptiles"))) {
            this.invalidCategoryCounter++;
        }
    }
}
