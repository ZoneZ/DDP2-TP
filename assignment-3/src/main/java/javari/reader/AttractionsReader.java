package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javari.park.CircleOfFire;
import javari.park.CountingMasters;
import javari.park.DancingAnimals;
import javari.park.Park;
import javari.park.PassionateCoders;

/**
 * Class that process an animals_attractions.csv file
 */
public class AttractionsReader extends CsvReader {

    private long validAttractionCounter;
    private long invalidAttractionCounter;
    private static List<String> attractionsName = new ArrayList<>();

    public long getValidAttractionCounter() {
        return validAttractionCounter;
    }

    public long getInvalidAttractionCounter() {
        return invalidAttractionCounter;
    }

    public static List<String> getAttractionsName() {
        return attractionsName;
    }

    public AttractionsReader(Path file) throws IOException {
        super(file);
        this.validAttractionCounter = 0;
        this.invalidAttractionCounter = 0;
    }

    public void lineProcessor(String line) {
        String[] lineSplit = line.split(",");
        String animalType = lineSplit[0];
        String attraction = lineSplit[1];

        if (!AttractionsReader.attractionsName.contains(attraction)) {
            AttractionsReader.attractionsName.add(attraction);
            this.validAttractionCounter++;
        }

        if (attraction.equalsIgnoreCase("Circle of Fire")
                && (animalType.equals("Whale") || animalType.equals("Lion")
                    || animalType.equals("Eagle"))) {
            Park.addAttraction(new CircleOfFire(animalType));

        } else if (attraction.equalsIgnoreCase("Dancing Animals")
                && (animalType.equals("Cat") || animalType.equals("Snake")
                    || animalType.equals("Parrot") || animalType.equals("Hamster"))) {
            Park.addAttraction(new DancingAnimals(animalType));

        } else if (attraction.equalsIgnoreCase("Counting Masters")
                && (animalType.equals("Hamster") || animalType.equals("Whale")
                    || animalType.equals("Parrot"))) {
            Park.addAttraction(new CountingMasters(animalType));

        } else if (attraction.equalsIgnoreCase("Passionate Coders")
                && (animalType.equals("Cat") || animalType.equals("Hamster")
                    || animalType.equals("Snake"))) {
            Park.addAttraction(new PassionateCoders(animalType));

        } else if (!(attraction.equalsIgnoreCase("Circle of Fire"))
                && !(attraction.equalsIgnoreCase("Dancing Animals"))
                && !(attraction.equalsIgnoreCase("Counting Masters"))
                && !(attraction.equalsIgnoreCase("Passionate Coders"))) {
            this.invalidAttractionCounter++;

        } else {
            this.invalidAttractionCounter++;
        }
    }
}
