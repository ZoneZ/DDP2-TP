package memorygame.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import memorygame.component.AnimalCard;
import memorygame.exceptions.ImageNotFoundException;

/**
 * This class represents the main window of the game.
 * All of the GUI components are constructed here.
 *
 * @author Farhan Azyumardhi Azmi - 1706979234
 * - Cover taken from https://raidenzein.deviantart.com/art/Doubutsu-Sentai-Zyuohger-Logo-591108396,
 * all rights reserved.
 * - Animal icons taken from https://www.flaticon.com/packs/animals-4,
 *   author https://www.flaticon.com/authors/roundicons, all rights reserved.
 */
public class GameWindow extends JFrame {
    /**
     * Number of row and collumn of the cards.
     */
    private final static int CARD_ROWS_COLLUMNS = 6;

    /**
     * Width of the cards panel.
     */
    private final static int CARDS_PANEL_WIDTH = 900;

    /**
     * Height of the cards panel.
     */
    private final static int CARDS_PANEL_HEIGHT = 600;

    /**
     * Width of each card.
     */
    private final static int CARD_WIDTH = 140;

    /**
     * Height of each card.
     */
    private final static int CARD_HEIGHT = 90;

    /**
     * Width of the tries panel.
     */
    private final static int TRIES_PANEL_WIDTH = 160;

    /**
     * Height of the tries panel.
     */
    private final static int TRIES_PANEL_HEIGHT = 140;

    /**
     * Font size.
     */
    private final static int FONT_SIZE = 16;

    /**
     * Width of the tries text field.
     */
    private final static int TRIES_TEXT_FIELD_WIDTH = 100;

    /**
     * Height of the tries text field.
     */
    private final static int TRIES_TEXT_FIELD_HEIGHT = 25;

    /**
     * Width of the status label.
     */
    private final static int STATUS_LABEL_WIDTH = 150;

    /**
     * Heigth of the status label.
     */
    private final static int STATUS_LABEL_HEIGHT = 40;

    /**
     * Width of the buttons panel.
     */
    private final static int BUTTON_PANEL_WIDTH = 160;

    /**
     * Heigth of the buttons panel.
     */
    private final static int BUTTON_PANEL_HEIGHT = 70;

    /**
     * Number of distinct cards.
     */
    private final static int NUM_OF_DISTINCT_CARDS = 18;

    /**
     * Number of total cards that are available in the game.
     */
    private final static int NUM_OF_TOTAL_CARDS = 36;

    /**
     * Constraints used to manage the GridBagLayout.
     */
    private GridBagConstraints gridConstraints;

    /**
     * The panel that holds all 36 cards.
     */
    private JPanel cardsPanel;

    /**
     * The panel that holds information of how many
     * attempts the user has taken in matching the cards.
     */
    private JPanel triesPanel;

    /**
     * The panel that holds the Start/Stop and Exit button.
     */
    private JPanel buttonsPanel;

    /**
     * The tries label.
     */
    private JLabel triesLabel;

    /**
     * The label that tracks how many attempts the user
     * has taken in matching the cards.
     */
    private JLabel triesNumberLabel;

    /**
     * The label that show the status of the game.
     */
    private JLabel statusLabel;

    /**
     * The button to start or stop the game.
     */
    private JButton startStopButton;

    /**
     * The button to exit the game.
     */
    private JButton exitButton;

    /**
     * ArrayList that holds all the 36 cards object.
     */
    private ArrayList<AnimalCard> cards;

    /**
     * Obtains the number of distinct cards.
     *
     * @return number of distinct cards.
     */
    public static int getNumOfDistinctCards() {
        return NUM_OF_DISTINCT_CARDS;
    }

    /**
     * Obtains the number of total cards available.
     *
     * @return number of total cards.
     */
    public static int getNumOfTotalCards() {
        return NUM_OF_TOTAL_CARDS;
    }

    /**
     * Obtains the cards panel.
     *
     * @return the cards panel.
     */
    public JPanel getCardsPanel() {
        return this.cardsPanel;
    }

    /**
     * Set or change the cards panel.
     *
     * @param cardsPanel : a panel that will set the cardsPanel variable value.
     */
    public void setCardsPanel(JPanel cardsPanel) {
        this.cardsPanel = cardsPanel;
    }

    /**
     * Obtains the tries number label.
     *
     * @return the tries number label.
     */
    public JLabel getTriesNumberLabel() {
        return this.triesNumberLabel;
    }

    /**
     * Obtains the status label.
     *
     * @return the status label.
     */
    public JLabel getStatusLabel() {
        return this.statusLabel;
    }

    /**
     * Obtains the start/stop button.
     *
     * @return the start/stop button.
     */
    public JButton getStartStopButton() {
        return this.startStopButton;
    }

    /**
     * Obtains the exit button.
     *
     * @return the exit button.
     */
    public JButton getExitButton() {
        return this.exitButton;
    }

    /**
     * Obtains the AnimalCard ArrayList.
     *
     * @return the AnimalCard ArrayList.
     */
    public ArrayList<AnimalCard> getCards() {
        return this.cards;
    }

    /**
     * Sets the start/stop button to a function.
     *
     * @param listener
     */
    public void setStartStopButtonFunction(ActionListener listener) {
        this.startStopButton.addActionListener(listener);
    }

    /**
     * Sets the exit button to a function.
     *
     * @param listener
     */
    public void setExitButtonFunction(ActionListener listener) {
        this.exitButton.addActionListener(listener);
    }

    /**
     * Create AnimalCard objects.
     */
    public void createCards() throws ImageNotFoundException {
        // Attempts to find "cover.png" and set it as a global cover for the AnimalCard objects.
        try {
            File cover = new File("assignment-4/src/main/java/memorygame/assets/cover.png");
            if (!cover.exists()) {
                throw new ImageNotFoundException("Image not found.");
            }
            AnimalCard.setCover(new ImageIcon(cover.getAbsolutePath()));

        } catch (ImageNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Cover not found");
        }

        /* Attempts to create 36 AnimalCard objects while also tries to find each
         * corresponding image file.
         */
        for (int i = 0; i < NUM_OF_TOTAL_CARDS; i++) {
            try {
                File icon;
                AnimalCard newCard;
                if (i < NUM_OF_DISTINCT_CARDS) {
                    icon = new File("assignment-4/src/main/java/memorygame/assets/"
                            + String.valueOf(i) + ".png");
                    if (!icon.exists()) {
                        throw new ImageNotFoundException("Image not found.");
                    }
                    newCard = new AnimalCard(i, new ImageIcon(icon.getAbsolutePath()));
                } else {
                    icon = new File("assignment-4/src/main/java/memorygame/assets/"
                            + String.valueOf(i - NUM_OF_DISTINCT_CARDS)
                            + ".png");
                    if (!icon.exists()) {
                        throw new ImageNotFoundException("Image not found.");
                    }
                    newCard = new AnimalCard(i - NUM_OF_DISTINCT_CARDS,
                            new ImageIcon(icon.getAbsolutePath()));
                }
                newCard.setPreferredSize(new Dimension(CARD_WIDTH, CARD_HEIGHT));
                this.cards.add(newCard);
            } catch (ImageNotFoundException e) {
                JOptionPane.showMessageDialog(null,
                        "Animal image not found.");
                System.exit(0);
            }
        }
    }

    /**
     * Create the cards panel.
     *
     * @return JPanel object as the cards panel.
     */
    public JPanel createCardsPanel() {
        JPanel cardsPanel = new JPanel();
        cardsPanel.setPreferredSize(new Dimension(CARDS_PANEL_WIDTH, CARDS_PANEL_HEIGHT));
        cardsPanel.setLayout(new GridBagLayout());

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 0;
        this.gridConstraints.gridy = 0;
        this.gridConstraints.gridheight = CARD_ROWS_COLLUMNS;

        this.getContentPane().add(cardsPanel, this.gridConstraints);


        for (int i = 0; i < NUM_OF_TOTAL_CARDS; i++) {
            JPanel singleCardPanel = new JPanel();
            singleCardPanel.setLayout(new GridLayout());
            singleCardPanel.setPreferredSize(new Dimension(CARD_WIDTH, CARD_HEIGHT));
            this.gridConstraints = new GridBagConstraints();
            this.gridConstraints.gridx = i % CARD_ROWS_COLLUMNS;
            this.gridConstraints.gridy = i / CARD_ROWS_COLLUMNS;
            this.gridConstraints.insets = new Insets(CARD_ROWS_COLLUMNS, CARD_ROWS_COLLUMNS, 0, 0);

            if (gridConstraints.gridx == CARD_ROWS_COLLUMNS - 1) {
                gridConstraints.insets = new Insets(CARD_ROWS_COLLUMNS, CARD_ROWS_COLLUMNS,
                        0, CARD_ROWS_COLLUMNS);
            }

            if (gridConstraints.gridy == CARD_ROWS_COLLUMNS - 1) {
                gridConstraints.insets = new Insets(CARD_ROWS_COLLUMNS, CARD_ROWS_COLLUMNS,
                        CARD_ROWS_COLLUMNS, 0);
            }

            if ((gridConstraints.gridx == (CARD_ROWS_COLLUMNS - 1)) &&
                    (gridConstraints.gridy == (CARD_ROWS_COLLUMNS - 1))) {
                gridConstraints.insets = new Insets(CARD_ROWS_COLLUMNS, CARD_ROWS_COLLUMNS,
                        CARD_ROWS_COLLUMNS, CARD_ROWS_COLLUMNS);
            }
            singleCardPanel.add(this.cards.get(i));
            cardsPanel.add(singleCardPanel, this.gridConstraints);
            this.cards.get(i).setIcon(AnimalCard.getCover());
        }
        return cardsPanel;
    }

    /**
     * Create the tries panel.
     *
     * @return the JPanel object as the tries panel.
     */
    public JPanel createTriesInfo() {
        JPanel triesPanel = new JPanel();
        triesPanel.setPreferredSize(new Dimension(TRIES_PANEL_WIDTH, TRIES_PANEL_HEIGHT));
        triesPanel.setLayout(new GridBagLayout());
        this.triesLabel = new JLabel("Number of Tries: ");
        this.triesLabel.setFont(new Font("Arial", Font.BOLD, FONT_SIZE));
        this.triesNumberLabel = new JLabel("0");
        this.triesNumberLabel.setPreferredSize(new Dimension(TRIES_TEXT_FIELD_WIDTH,
                TRIES_TEXT_FIELD_HEIGHT));
        this.triesNumberLabel.setFont(new Font("Arial", Font.BOLD, FONT_SIZE));
        this.triesNumberLabel.setBackground(Color.WHITE);
        this.triesNumberLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.statusLabel = new JLabel();
        this.statusLabel.setPreferredSize(new Dimension(STATUS_LABEL_WIDTH, STATUS_LABEL_HEIGHT));
        this.statusLabel.setOpaque(true);
        this.statusLabel.setBackground(Color.ORANGE);
        this.statusLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.statusLabel.setFont(new Font("Arial", Font.PLAIN, FONT_SIZE));


        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 1;
        this.gridConstraints.gridy = 0;

        this.getContentPane().add(triesPanel, this.gridConstraints);

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 0;
        this.gridConstraints.gridy = 0;
        triesPanel.add(this.triesLabel, this.gridConstraints);

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 0;
        this.gridConstraints.gridy = 1;
        triesPanel.add(this.triesNumberLabel, this.gridConstraints);

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 0;
        this.gridConstraints.gridy = 2;
        triesPanel.add(this.statusLabel, this.gridConstraints);

        return triesPanel;
    }

    /**
     * Create the buttons panel.
     *
     * @return the JPanel object as the buttons panel.
     */
    public JPanel createButtons() {
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setPreferredSize(new Dimension(BUTTON_PANEL_WIDTH, BUTTON_PANEL_HEIGHT));
        this.startStopButton = new JButton("Start Game");
        this.startStopButton.setFont(new Font("Arial", Font.BOLD, FONT_SIZE));
        this.exitButton = new JButton("Exit");
        this.exitButton.setFont(new Font("Arial", Font.BOLD, FONT_SIZE));

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 1;
        this.gridConstraints.gridy = 4;
        this.getContentPane().add(buttonsPanel, this.gridConstraints);

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 0;
        this.gridConstraints.gridy = 0;
        buttonsPanel.add(this.startStopButton, this.gridConstraints);

        this.gridConstraints = new GridBagConstraints();
        this.gridConstraints.gridx = 0;
        this.gridConstraints.gridy = 1;
        buttonsPanel.add(this.exitButton, this.gridConstraints);

        return buttonsPanel;
    }

    /**
     * Constructs the GUI. Calling all the methods to build its panels and components.
     */
    public GameWindow() {
        this.cards = new ArrayList<>();
        this.setTitle("Remember the Animal");
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new GridBagLayout());
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setBounds((int) (0.5 * (screenSize.width - this.getWidth())),
                (int) (0.5 * (screenSize.height - this.getHeight())),
                this.getWidth(), this.getHeight());
        this.createCards();
        Collections.shuffle(this.cards);
        this.cardsPanel = this.createCardsPanel();
        this.triesPanel = this.createTriesInfo();
        this.buttonsPanel = this.createButtons();
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}