package memorygame;

import memorygame.gui.GameController;
import memorygame.gui.GameWindow;

/**
 * Main class for Assignment 4.
 *
 * @author Farhan Azyumardhi Azmi - 1706979234
 */
public class A4MemoryGame {

    public static void main(String[] args) {
        GameWindow gameView = new GameWindow();
        GameController gameController = new GameController(gameView);
        gameController.run();
    }
}