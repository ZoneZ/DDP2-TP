package memorygame.component;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * This class represents a single animal card as an extension of JButton class.
 *
 * @author Farhan Azyumardhi Azmi - 1706979234
 */
public class AnimalCard extends JButton {

    /**
     * ID of a single card.
     */
    private int id;

    /**
     * Icon of a single card.
     */
    private ImageIcon animalIcon;

    /**
     * "Global" cover for all instances of this class.
     */
    private static ImageIcon cover;

    /**
     * Determines if a card is clickable or not.
     */
    private boolean clickable;

    /**
     * Check if a card is clickable or not.
     *
     * @return clickable value.
     */
    public boolean isClickable() {
        return this.clickable;
    }

    /**
     * Change the status of a card.
     *
     * @param clickable : the status to be applied.
     */
    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    /**
     * Obtains the ID of a card.
     *
     * @return ID of a card.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Obtains the icon of a card.
     *
     * @return the icon of a card.
     */
    public ImageIcon getAnimalIcon() {
        return this.animalIcon;
    }

    /**
     * Sets the global cover for every card.
     *
     * @param cover : cover to be applied.
     */
    public static void setCover(ImageIcon cover) {
        AnimalCard.cover = cover;
    }

    /**
     * Obtains the global cover.
     *
     * @return the cover.
     */
    public static ImageIcon getCover() {
        return AnimalCard.cover;
    }

    /**
     * Class constructor.
     *
     * @param id         : card ID.
     * @param animalIcon : card icon.
     */
    public AnimalCard(int id, ImageIcon animalIcon) {
        this.id = id;
        this.animalIcon = animalIcon;
        this.clickable = true;
    }

    /**
     * Passes an ActionListener object of a card to a method.
     *
     * @param listener
     */
    public void setAnimalCardFunction(ActionListener listener) {
        this.addActionListener(listener);
    }

    /**
     * Checks if this class instance has the same ID with the given card.
     *
     * @param other : another AnimalCard object.
     * @return : true if the two cards have the same ID, else if otherwise.
     */
    public boolean hasSameID(AnimalCard other) {
        return this.id == other.getId();
    }
}